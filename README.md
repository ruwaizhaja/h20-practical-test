# H20PracticalTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.



## Setting up the Project

- Install Node.JS – https://nodejs.org/en/download/
- Install Angular CLI – “$ npm install -g @angular/cli” or “npm install -g @angular/cli”
- Clone the project using this link "https://gitlab.com/ruwaizhaja/h20-practical-test.git" for example "git clone https://gitlab.com/ruwaizhaja/h20-practical-test.git"
- Or use this link to download the project "https://drive.google.com/file/d/1UtUmOgbpbmI0gWq5dWfFKPpNmPYjM9rr/view?usp=sharing"
- Go to the root folder of the project – “h20-practical-test  ”
- Open command prompt or the terminal and run “npm install”
- Once installed run “npm start” or “ng serve -o”
- Open a web browser and go to – “http://localhost:4200/”
- Now you will be able to view the project !

import { Component, ElementRef, Host, HostListener, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'h20-practical-test';

  addPopoverToggle: boolean = false;
  templateView: any;
  isNewContact: boolean = false;
  contactName = "";
  // Array of Contacts__
  contacts = ["Atticus Abishag","Lída Mari", "Ismene Cvijeta", "Jayashri Parry", "Ascanio Nikolay", "Evripidis Xenia", "Seraphina Rajesh"]

  @ViewChild("profileDetailsInfo") profileDetailsTemp: any;
  @ViewChild("profileDetailsNew") newProfileDetailsTemp: any;

  
  ngAfterViewInit(){
    // Loading default view on page load__
    this.templateView = this.profileDetailsTemp;
  }

  // Add new contact toggle ___
  togglePopover(event: any){
    event.stopPropagation();
    this.addPopoverToggle = !this.addPopoverToggle;
  }

  // Close Popover when clicked outside the element__
  @HostListener("document:click", ['$event'])
  onDocumentClick(event:any){
    this.addPopoverToggle = false;
  }

  // Change View to add new contact ___
  showNewProfileDetails(){ 
    this.templateView = this.newProfileDetailsTemp;
    this.isNewContact = true;
  }

  // When Saving New Contact ___
  onSave(){
    if(this.contactName != "") {
      this.contacts.push(this.contactName);
      this.contactName = "";
      this.templateView = this.profileDetailsTemp;
      this.isNewContact = false;
    };
  }
}
